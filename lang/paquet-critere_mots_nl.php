<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-critere_mots?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'critere_mots_description' => 'Laat toe elementen te tonen die meerdere trefwoorden gemeen hebben door middel van de omgevingsvariabele $mots[]',
	'critere_mots_slogan' => 'Objecten selecteren die gemeenschappelijke trefwoorden hebben'
);
