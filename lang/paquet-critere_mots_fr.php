<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/critere_mots.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'critere_mots_description' => 'Permet d’afficher les éléments qui ont plusieurs mots clés en communs, via la variable $mots[] passée dans l’environnement',
	'critere_mots_slogan' => 'Sélectionner des objets ayant des mots clés en communs'
);
